# Run "nim c mean_func_1.nim"

import nimterop/cimport
import os
from macros import getProjectPath

static:
  cDisableCaching()
  cDebug()

const
  includePath = getProjectPath() / "matlab_export"
static:
  doAssert fileExists(includePath / "rtwtypes.h")
  doAssert fileExists(includePath / "mean_func_1_types.h")
  doAssert fileExists(includePath / "mean_func_1.h")
  # Put cAddSearchDir in static block: https://github.com/nimterop/nimterop/issues/122
  cAddSearchDir(includePath)

cImport(cSearchPath("mean_func_1.h"), recurse=true)
